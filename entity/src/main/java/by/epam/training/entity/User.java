package by.epam.training.entity;

import java.sql.Date;

public class User implements IdsEntity  {
	private Long id;
	private String name;
	private String gender;
	private Date born;

	public User() {
	}

	public User(Long id, String name, String gender, Date born) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.born = born;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBorn() {
		return born;
	}

	public void setBorn(Date born) {
		this.born = born;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", gender='" + gender + '\'' +
				", born=" + born +
				'}';
	}
}
