package by.epam.training.client.common;

import by.epam.training.client.IElasticClient;
import by.epam.training.entity.IdsEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class AbstractElasticClient<T extends IdsEntity> implements IElasticClient<T> {

	protected final static Gson gson = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd")
			.create();
}
