package by.epam.training.client;

import by.epam.training.entity.IdsEntity;
import org.elasticsearch.index.query.QueryBuilder;

import java.io.IOException;

public interface IElasticClient<T extends IdsEntity> {

	void createIndex() throws IOException;

	void addMapping(String payload) throws IOException;

	void addAll(Iterable<T> items) throws IOException;

	Iterable<T> getAll() throws IOException;

	Iterable<T> search(QueryBuilder queryBuilder) throws IOException;
	long delete(QueryBuilder queryBuilder) throws IOException;
}
