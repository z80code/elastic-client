package by.epam.training.client.entity;

public class ResponseEntity<T> {
    private long took;
    private boolean timed_out;
   private Long deleted;
    private ShardsEntity _shards;
    private HitsEntity<T> hits;

    public ResponseEntity() {
    }

   public long getTook() {
      return took;
   }

   public void setTook(long took) {
      this.took = took;
   }

   public boolean isTimed_out() {
      return timed_out;
   }

   public void setTimed_out(boolean timed_out) {
      this.timed_out = timed_out;
   }

   public ShardsEntity get_shards() {
      return _shards;
   }

   public void set_shards(ShardsEntity _shards) {
      this._shards = _shards;
   }

   public HitsEntity<T> getHits() {
      return hits;
   }

   public void setHits(HitsEntity<T> hits) {
      this.hits = hits;
   }

   public Long getDeleted() {
      return deleted;
   }

   public void setDeleted(Long deleted) {
      this.deleted = deleted;
   }
}
