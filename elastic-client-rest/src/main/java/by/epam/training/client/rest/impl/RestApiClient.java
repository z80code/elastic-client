package by.epam.training.client.rest.impl;

import by.epam.training.client.common.AbstractElasticClient;
import by.epam.training.client.entity.HitsEntity;
import by.epam.training.client.entity.ResourceEntity;
import by.epam.training.client.entity.ResponseEntity;
import by.epam.training.entity.IdsEntity;
import by.epam.training.entity.User;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilder;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.*;

public class RestApiClient<T extends IdsEntity> extends AbstractElasticClient<T> implements Closeable {

	private static final String ADDRESS_DEFAULT = "localhost";
	private static final int REST_PORT = 9200;
	private static final String REST_PROTOCOL = "http";

	private static final String INDEX_DEFAULT = "index";
	private static final String TYPE_DEFAULT = "type";


	private RestClient restClient;

	private String index;
	private String type;

	private Map<String, String> params;

	private Class<T> classOfT;

	public RestApiClient(String host, String index, String type, Class<T> classOfT) {

		Header[] headers = {new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json")};
		this.restClient =
				RestClient.builder(
						new HttpHost(host, REST_PORT, REST_PROTOCOL))
						.setDefaultHeaders(headers)
						.build();
		this.index = index;
		this.type = type;
		this.classOfT = classOfT;
		this.params = new LinkedHashMap<>();
//		params.put("filter_path", "hits.hits._source");
	}


	@Override
	public void createIndex() throws IOException {

		Settings indexSettings = Settings.builder()
				.put("index.number_of_shards", 1)
				.put("index.number_of_replicas", 0)
				.build();
		String payload = XContentFactory.jsonBuilder()
				.startObject()
				.startObject("settings")
				.value(indexSettings)
				.endObject()
				.endObject().string();
		HttpEntity entity = new NStringEntity(payload, ContentType.APPLICATION_JSON);
		Response response = restClient.performRequest(
				HttpMethod.PUT,
				index,
				params,
				entity
		);
		//	TODO Handle response status.
	}

	@Override
	public void addMapping(String payload) throws IOException {
		HttpEntity entity = new NStringEntity(payload, ContentType.APPLICATION_JSON);
		String endPoint = endPointGenerator(index, HttpOperation.MAPPING, type);
		Response response = restClient.performRequest(
				HttpMethod.PUT,
				endPoint,
				params,
				entity);
		//	TODO Handle response status.
	}

	@Override
	public void addAll(Iterable<T> items) throws IOException {

		String endPoint = endPointGenerator(index, type, HttpOperation.BULK);

		StringBuilder sb = new StringBuilder();
		for (T item : items) {
			sb.append("{ \"index\":{\"_id\": ")
					.append(item.getId())
					.append("} }\n");
			sb.append(gson.toJson(item))
					.append("\n");
		}

		HttpEntity entity = new NStringEntity(sb.toString(), ContentType.APPLICATION_JSON);
		Response response = restClient.performRequest(
				HttpMethod.POST,
				endPoint,
				params,
				entity);
		//	TODO Handle response status.
	}

	@Override
	public Iterable<T> getAll() throws IOException {
		String endPoint = endPointGenerator(index, type, HttpOperation.SEARCH);

		final String bodySelectAll = "{\"query\":{\"match_all\":{} }}";

		HttpEntity entity = new NStringEntity(bodySelectAll, ContentType.APPLICATION_JSON);
		Response response = restClient.performRequest(
				HttpMethod.POST,
				endPoint,
				params,
				entity);
		//	TODO Handle response status.
		return getItemsFromEntity(response.getEntity());
	}

	@Override
	public Iterable<T> search(QueryBuilder queryBuilder) throws IOException {
		String endPoint = endPointGenerator(index, type, HttpOperation.SEARCH);
		String querySearch = XContentFactory.jsonBuilder()
				.startObject()
				.field("query", queryBuilder)
				.endObject()
				.string();

		HttpEntity entity = new NStringEntity(querySearch, ContentType.APPLICATION_JSON);
		Response response = restClient.performRequest(
				HttpMethod.POST,
				endPoint,
				params,
				entity);
		//	TODO Handle response status.
		return getItemsFromEntity(response.getEntity());
	}

	@Override
	public long delete(QueryBuilder queryBuilder) throws IOException {
		String endPoint = endPointGenerator(index, type, HttpOperation.DELETE_BY_QUERY);
		String delete_by_query = XContentFactory.jsonBuilder()
				.startObject()
				.field("query", queryBuilder)
				.endObject()
				.string();
		HttpEntity entity = new NStringEntity(delete_by_query, ContentType.APPLICATION_JSON);
		Response response = restClient.performRequest(
				HttpMethod.POST,
				endPoint,
				params,
				entity);
		//	TODO Handle response status.
		return getDeletedCountFromEntity(response.getEntity());
	}

	@Override
	public void close() throws IOException {
		restClient.close();
	}

	private static String endPointGenerator(String... paths) {
		StringBuilder sb = new StringBuilder();
		for (String path : paths) {
			sb.append(path).append("/");
		}
		sb.deleteCharAt(sb.lastIndexOf("/"));
		return sb.toString();
	}

	protected interface HttpOperation {
		String BULK = "_bulk";
		String SEARCH = "_search";
		String DELETE = "_delete";
		String DELETE_BY_QUERY = "_delete_by_query";
		String MAPPING = "_mapping";
	}

	protected interface HttpMethod {
		String GET = "GET";
		String POST = "POST";
		String PUT = "PUT";
		String DELETE = "DELETE";
	}

	private ResponseEntity<T> getResponseEntityFromHttpEntity(HttpEntity httpEntity) throws IOException {
		String responseBody = EntityUtils.toString(httpEntity);
		return gson.fromJson(responseBody, new TypeToken<ResponseEntity<User>>() {}.getType());
	}

	private Long getDeletedCountFromEntity(HttpEntity httpEntity) throws IOException {
		ResponseEntity<T> responseEntity = getResponseEntityFromHttpEntity(httpEntity);
		return responseEntity.getDeleted();
	}

	private List<T> getItemsFromEntity(HttpEntity httpEntity) throws IOException {
		ResponseEntity<T> responseEntity = getResponseEntityFromHttpEntity(httpEntity);
		List<T> items = new ArrayList<>();
		responseEntity.getHits().getHits().forEach(hit->{
			items.add(hit.get_source());
		});
		return items;
	}
}
