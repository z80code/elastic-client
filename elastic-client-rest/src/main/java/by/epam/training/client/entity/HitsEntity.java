package by.epam.training.client.entity;

import java.util.List;

public class HitsEntity<T> {
    private long total;
    private Double max_score;
    private long deleted;
    private List<ResourceEntity<T>> hits;

    public HitsEntity() {
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public Double getMax_score() {
        return max_score;
    }

    public void setMax_score(Double max_score) {
        this.max_score = max_score;
    }

    public long getDeleted() {
        return deleted;
    }

    public void setDeleted(long deleted) {
        this.deleted = deleted;
    }

    public List<ResourceEntity<T>> getHits() {
        return hits;
    }

    public void setHits(List<ResourceEntity<T>> hits) {
        this.hits = hits;
    }
}
