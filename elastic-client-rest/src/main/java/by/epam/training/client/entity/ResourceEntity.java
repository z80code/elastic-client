package by.epam.training.client.entity;

public class ResourceEntity<T> {

    private String _index;
    private String _type;
    private Long _id;
    private Long _version;
    private boolean found;
    private T _source;
    private String result;

    public ResourceEntity() {
    }

    public String get_index() {
        return _index;
    }

    public void set_index(String _index) {
        this._index = _index;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public Long get_version() {
        return _version;
    }

    public void set_version(Long _version) {
        this._version = _version;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public T get_source() {
        return _source;
    }

    public void set_source(T _source) {
        this._source = _source;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
