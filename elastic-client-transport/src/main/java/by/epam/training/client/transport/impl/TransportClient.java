package by.epam.training.client.transport.impl;

import by.epam.training.client.common.AbstractElasticClient;
import by.epam.training.entity.IdsEntity;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class TransportClient<T extends IdsEntity> extends AbstractElasticClient<T> implements Closeable {

	private static final int TRANSPORT_PORT = 9300;

	private static final String TRANSPORT_ADDRESS_DEFAULT = "localhost";
	private static final String CLUSTER_NAME_DEFAULT = "elasticsearch";

	private static final String INDEX_DEFAULT = "index";
	private static final String TYPE_DEFAULT = "type";

	private static final Class CLASS_DEFAULT = Object.class;

	private Client client;

	private String index;
	private String type;
	private Class<T> classOfT;
	private IndicesAdminClient indicesAdminClient;

	public TransportClient() throws UnknownHostException {
		this(TRANSPORT_ADDRESS_DEFAULT, CLUSTER_NAME_DEFAULT, INDEX_DEFAULT, TYPE_DEFAULT, CLASS_DEFAULT);
	}

	public TransportClient(String host, String clusterName, String index, String type, Class<T> classOfT) throws UnknownHostException {

		Settings settings = Settings.builder()
				.put("cluster.name", clusterName).build();
		this.client = new PreBuiltTransportClient(settings)
				.addTransportAddress(new TransportAddress(InetAddress.getByName(host), TRANSPORT_PORT));
		this.index = index;
		this.type = type;
		this.classOfT = classOfT;
	}

	@Override
	public void createIndex() {

		IndicesAdminClient adminClient = client.admin().indices();

		if (adminClient.prepareExists(index).get().isExists()) {
			adminClient.prepareDelete(index).get();
		}

		// TODO Settings create initialised by some method
		// Here is only for economy using of the memory
		Settings indexSettings = Settings.builder()

				.put("index.number_of_shards", 1)
				.put("index.number_of_replicas", 0)
				.build();

		indicesAdminClient = client.admin().indices();

		// Create new index
		indicesAdminClient
				.prepareCreate(index)
				.setSettings(indexSettings)
				.get();
		//	TODO Handle response status.
	}

	@Override
	public void addMapping(String payload) throws IOException {
		if (indicesAdminClient == null) {
			throw new IllegalStateException("Index not created yet.");
		}
		indicesAdminClient.preparePutMapping(index)
				.setType(type)
				.setSource(payload, XContentType.JSON)
				.get();
		//	TODO Handle response status.
	}

	@Override
	public void addAll(Iterable<T> items) {
		BulkRequestBuilder bulkRequest = client.prepareBulk();
		for (T item : items) {
			bulkRequest.add(client
					.prepareIndex(index, type)
					.setSource(gson.toJson(item), XContentType.JSON));
		}

		BulkResponse bulkResponse = bulkRequest.get();
		//	TODO Handle response status.
//       if (bulkResponse.hasFailures()) {
//			System.out.println("Can`t create list of items.");
//		}
	}

	@Override
	public Iterable<T> getAll() {
		QueryBuilder querySelectAllUsers = QueryBuilders.matchAllQuery();
		SearchResponse getAllResponse = client
				.prepareSearch(index)
				.setQuery(querySelectAllUsers)
				.get();
		//	TODO Handle response status.
		return getAsList(getAllResponse, classOfT);
	}

	@Override
	public Iterable<T> search(QueryBuilder queryBuilder) {
		SearchResponse searchResponse = client
				.prepareSearch(index)
				.setQuery(queryBuilder)
				.get();
		//	TODO Handle response status.
		return getAsList(searchResponse, classOfT);
	}

	@Override
	public long delete(QueryBuilder queryBuilder) {
		BulkByScrollResponse response =
				DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
						.filter(queryBuilder)
						.source(index)
						.get();
		//	TODO Handle response status.
		return response.getDeleted();
	}


	private List<T> getAsList(SearchResponse sr, Class<T> classOfT) {
		SearchHit[] results = sr.getHits().getHits();
		List<T> userList = new ArrayList<>(results.length);
		for (SearchHit hit : results) {
			String sourceAsString = hit.getSourceAsString();
			if (sourceAsString != null) {
				userList.add(gson.fromJson(sourceAsString, classOfT));
			}
		}
		return userList;
	}

	@Override
	public void close() throws IOException {
		client.close();
	}
}
