import java.io.IOException;
import java.sql.Date;
import java.util.*;
import java.util.concurrent.TimeUnit;

import by.epam.training.client.rest.impl.RestApiClient;
import by.epam.training.client.transport.impl.TransportClient;
import by.epam.training.entity.User;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

public class Runner {

	private final static List<User> users = Arrays.asList(
			new User(1L, "Louise Greer", "female", Date.valueOf("1996-05-29")),
			new User(2L, "Castillo Jones", "male", Date.valueOf("2001-01-25")),
			new User(3L, "Kirby Soda", "male", Date.valueOf("1991-08-17")),
			new User(4L, "Lindsey Cobb", "female", Date.valueOf("1994-07-15")),
			new User(5L, "Estelle Suarez", "female", Date.valueOf("1993-03-02")),
			new User(6L, "Rachael Terrell", "female", Date.valueOf("1997-04-09")),
			new User(7L, "Bob Dillan", "male", Date.valueOf("2001-08-17"))
	);

	private static void printUsers(Iterable<User> userList) {
		for (User user : userList) {
			System.out.println(user);
		}
	}

	public static void main(String[] args) throws IOException {
		RestApiClient<User> restClient = null;
		TransportClient<User> transportClient = null;

		// Host setting
		String host = "localhost";
		String clusterName = "elasticsearch";
		String restIndex = "mail";
		String transportIndex = "forum";
		String type = "user";

		try {

			restClient =
					new RestApiClient<>(
							host,
							restIndex,
							type,
							User.class);

			transportClient =
					new TransportClient<>(
							host,
							clusterName,
							transportIndex,
							type,
							User.class);

			// Create new index
			transportClient.createIndex();
			restClient.createIndex();

			// Apply new mapping to the previously created index
			// Custom json
			String payload = XContentFactory.jsonBuilder()
					.startObject()
					.startObject(type)
					.startObject("properties")
					.startObject("id")
					.field("type", "long")
					.endObject()
					.startObject("name")
					.field("type", "text")
					.endObject()
					.startObject("gender")
					.field("type", "text")
					.endObject()
					.startObject("born")
					.field("type", "date")
					.field("format", "yyyy-MM-dd")
					.endObject()
					.endObject()
					.endObject()
					.endObject().string();

			transportClient.addMapping(payload);
			restClient.addMapping(payload);

			// Pause while elasticsearch processing operation.
			TimeUnit.SECONDS.sleep(2);

			// Create User class and store JSON representation of user instances into ElasticSearch
			transportClient.addAll(users);
			restClient.addAll(users);

			// Pause while elasticsearch processing operation.
			TimeUnit.SECONDS.sleep(2);

			//	Perform search queries to select:

			//	- all users,
			Iterable<User> list = transportClient.getAll();
			printUsers(list);
			list = restClient.getAll();
			printUsers(list);
			//	- male users only

			QueryBuilder querySelectMaleOnly = QueryBuilders.boolQuery()
					.must(QueryBuilders.termQuery("gender", "male"));
			list = transportClient.search(querySelectMaleOnly);
			printUsers(list);
			list = restClient.search(querySelectMaleOnly);
			printUsers(list);

			//	- male users with name XXX
			QueryBuilder query3 = QueryBuilders.boolQuery()
					.must(QueryBuilders.matchQuery("name", "Bob Dillan"))
					.must(QueryBuilders.matchQuery("gender", "male"));
			list = transportClient.search(query3);
			printUsers(list);
			list = restClient.search(query3);
			printUsers(list);

			//	- male users which were born after date xx-xx-xx and with name XXX
			QueryBuilder query4 = QueryBuilders.boolQuery()
					.must(QueryBuilders.matchQuery("name", "Bob Dillan"))
					.must(QueryBuilders.matchQuery("gender", "male"))
					.must(QueryBuilders.rangeQuery("born").gte("12-10-2000").format("dd-MM-yyyy"));
			list = transportClient.search(query4);
			printUsers(list);
			list = restClient.search(query4);
			printUsers(list);

			// Delete all male users (BULK API) using ONE request only.

			long deleted = transportClient.delete(QueryBuilders.matchQuery("gender", "male"));
			System.out.println(deleted);
			deleted = restClient.delete(QueryBuilders.matchQuery("gender", "male"));
			System.out.println(deleted);
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			if (transportClient != null) {
				transportClient.close();
			}
			if (transportClient != null) {
				restClient.close();
			}
		}
	}
}
